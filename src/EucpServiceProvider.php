<?php

namespace Ardon\EucpSms;

use Illuminate\Support\ServiceProvider;

class EucpServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/eucpsms.php' => config_path('eucpsms.php'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton('eucpsms', function (){
            return new Eucp();
        });
    }
}
