<?php
/*
 * config file for eucp
 */
return [
    "ym_sms_addr"                      => "www.btom.cn:8080", /*接口地址,请联系销售获取*/
    "ym_sms_send_uri"                  => "/inter/sendSingleSMS", /*发送单条短信接口*/
    "ym_sms_send_batch_uri"            => "/inter/sendBatchSMS", /*发送批次短信接口*/
    "ym_sms_send_batchonly_sms_uri"    => "/inter/sendBatchOnlySMS", /*发送批次[不支持自定义smsid]短信接口*/
    "ym_sms_send_personality_sms_uri"  => "/inter/sendPersonalitySMS", /*发送个性短信接口*/
    "ym_sms_getreport_uri"             => "/inter/getReport", /*获取状态报告接口*/
    "ym_sms_getmo_uri"                 => "/inter/getMo", /*获取上行接口*/
    "ym_sms_getbalance_uri"            => "/inter/getBalance",   /*获取余额接口*/
    "ym_sms_appid"                     => "EUCP-EMY-SMS1-XXXXX", /*APPID,请联系销售或者在页面获取*/
    "ym_sms_aespwd"                    => "1234567890123456", /*密钥，请联系销售或者在页面获取*/
    "en_gzip"                          => false, /* 是否开启GZIP */
    "end"                              => "\n",
    "iv"                               => "", //密钥偏移量IV，可自定义
    'encryptkey'                       => ""
];