# Eucp for Laravel5.5/5.6
>亿美短信平台接口

## Installation/安装
```shell
composer require ardon/eucp-sms
```

### laravel 5.*
> 在laravel config/app.php里面的providers添加下面的代码

```php
Ardon\EucpSms\EucpServiceProvider::class
```

### 发布laravel扩展包
> 生成一个配置文件eucpsms.php 在 config文件夹下面

```shell
php artisan vendor:publish --provider="Ardon\EucpSms\EucpServiceProvider"
```

## 配置

### 修改config/eucpsms.php文件内容

```php
return [
    "ym_sms_addr"                      => "www.btom.cn:8080", /*接口地址,请联系销售获取*/
    "ym_sms_send_uri"                  => "/inter/sendSingleSMS", /*发送单条短信接口*/
    "ym_sms_send_batch_uri"            => "/inter/sendBatchSMS", /*发送批次短信接口*/
    "ym_sms_send_batchonly_sms_uri"    => "/inter/sendBatchOnlySMS", /*发送批次[不支持自定义smsid]短信接口*/
    "ym_sms_send_personality_sms_uri"  => "/inter/sendPersonalitySMS", /*发送个性短信接口*/
    "ym_sms_getreport_uri"             => "/inter/getReport", /*获取状态报告接口*/
    "ym_sms_getmo_uri"                 => "/inter/getMo", /*获取上行接口*/
    "ym_sms_getbalance_uri"            => "/inter/getBalance",   /*获取余额接口*/
    "ym_sms_appid"                     => "EUCP-EMY-SMS1-ABCDE", /*APPID,请联系销售或者在页面获取*/
    "ym_sms_aespwd"                    => "89105A4C15CABCDE", /*密钥，请联系销售或者在页面获取*/
    "en_gzip"                          => true, /* 是否开启GZIP */
    "end"                              => "\n",
    "iv"                               => "", //密钥偏移量IV，可自定义
    "ym_sms_sign"                      => "【短信签名】",
];
```
## 使用

```php
use Ardon\EucpSms\Eucp;

$result = Eucp::SendSMS($phone, $content);
```
